module.exports = {
  siteMetadata: {
    // siteUrl: "https://blog.btaskee.com",
    title: "Blog bTaskee",
  },
  plugins: [
    "gatsby-plugin-image",
    "gatsby-plugin-sharp",
  ],
};
